name := "PlayJavaStarter"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.8"

libraryDependencies += javaJdbc
libraryDependencies += cache
libraryDependencies += javaWs
libraryDependencies += filters
libraryDependencies += evolutions

// https://mvnrepository.com/artifact/io.swagger/swagger-play2_2.11
libraryDependencies += "io.swagger" %% "swagger-play2" % "1.5.3"

// https://mvnrepository.com/artifact/io.dropwizard.metrics
libraryDependencies += "io.dropwizard.metrics" % "metrics-core" % "3.2.2"
libraryDependencies += "io.dropwizard.metrics" % "metrics-json" % "3.2.2"
libraryDependencies += "io.dropwizard.metrics" % "metrics-jvm" % "3.2.2"
libraryDependencies += "io.dropwizard.metrics" % "metrics-healthchecks" % "3.2.2"