package it.szarama.http;

import com.google.inject.Inject;
import play.http.DefaultHttpFilters;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 02.02.2017.
 */
public class Filters extends DefaultHttpFilters {

    @Inject
    public Filters(MetricsFilter metricsFilter) {
        super(metricsFilter);
    }
}
