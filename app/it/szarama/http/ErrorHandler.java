package it.szarama.http;

import it.szarama.utils.JsonResponse;
import play.Logger;
import play.http.HttpErrorHandler;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 18.01.2017.
 */
public class ErrorHandler implements HttpErrorHandler {

    @Override
    public CompletionStage<Result> onClientError(Http.RequestHeader request, int statusCode, String message) {
        Logger.error("Client error (" + statusCode + "): " + message + " at " + request.uri());

        JsonResponse.Response<String> responseDto = new JsonResponse.Response<>();
        responseDto.setError(true);
        responseDto.setMessage(message);

        return CompletableFuture.completedFuture(Results.status(statusCode, Json.toJson(responseDto)));
    }

    @Override
    public CompletionStage<Result> onServerError(Http.RequestHeader request, Throwable exception) {
        Logger.error("Server error: " + exception.getMessage() + " at " + request.uri());

        JsonResponse.Response<String> responseDto = new JsonResponse.Response<>();
        responseDto.setError(true);
        responseDto.setMessage(exception.getMessage());

        return CompletableFuture.completedFuture(Results.internalServerError(Json.toJson(responseDto)));
    }
}
