package it.szarama.http;

import akka.stream.Materializer;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.inject.Inject;
import it.szarama.metrics.MetricsService;
import play.mvc.Filter;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 02.02.2017.
 */
public class MetricsFilter extends Filter {

    private final Timer requestTimer;
    private final Counter activeRequests;
    private final Meter errorStatuses;

    @Inject
    public MetricsFilter(MetricsService metricsService, Materializer mat) {
        super(mat);
        MetricRegistry metricsRegistry = metricsService.getMetricsRegistry();
        this.requestTimer = metricsRegistry.timer(MetricRegistry.name(this.getClass(), "requestTimer"));
        this.activeRequests = metricsRegistry.counter(MetricRegistry.name(this.getClass(), "activeRequests"));
        this.errorStatuses = metricsRegistry.meter(MetricRegistry.name(this.getClass(), "errorStatuses"));
    }

    @Override
    public CompletionStage<Result> apply(Function<Http.RequestHeader, CompletionStage<Result>> nextFilter, Http.RequestHeader requestHeader) {
        final Timer.Context timeContext = requestTimer.time();
        activeRequests.inc();

        return nextFilter.apply(requestHeader).thenApply(result -> {
            activeRequests.dec();
            long requestTime = TimeUnit.MILLISECONDS.convert(timeContext.stop(), TimeUnit.NANOSECONDS);
            if (result.status() == Http.Status.INTERNAL_SERVER_ERROR) errorStatuses.mark();
            return result.withHeader("Request-Time", requestTime + " ms");
        });
    }
}
