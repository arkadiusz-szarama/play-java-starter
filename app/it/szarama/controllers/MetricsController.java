package it.szarama.controllers;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.jvm.ThreadDump;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.szarama.metrics.MetricsService;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.SortedMap;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 05.04.2017.
 */
@Api(value = "/metrics", description = "Default controller")
public class MetricsController extends Controller {
    private final MetricsService metricsService;
    private final ObjectWriter objectWriter;
    private final Charset UTF_8 = Charset.forName("UTF-8");

    @Inject
    public MetricsController(MetricsService metricsService) {
        this.metricsService = metricsService;
        this.objectWriter = metricsService.getObjectWriter();
    }

    @ApiOperation("Json with all metrics information")
    public Result metrics() throws JsonProcessingException {
        return ok(objectWriter.writeValueAsString(metricsService.getMetricsRegistry()));
    }

    @ApiOperation("Json with health check results")
    public Result healthCheck() throws JsonProcessingException {
        HealthCheckRegistry healthCheckRegistry = metricsService.getHealthCheckRegistry();
        SortedMap<String, HealthCheck.Result> healthChecks = healthCheckRegistry.runHealthChecks(metricsService.getExecutorService());
        return ok(objectWriter.writeValueAsString(healthChecks));
    }

    @ApiOperation("Threads dump")
    public Result threadDump() throws JsonProcessingException {
        ThreadDump threadDump = metricsService.getThreadDump();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        threadDump.dump(outputStream);
        return ok(new String(outputStream.toByteArray(), UTF_8));
    }

    @ApiOperation("Ping")
    public Result ping() {
        return ok("pong");
    }
}
