package it.szarama.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 25.02.2017.
 */
@Api(value = "/", description = "Default controller")
public class ApplicationController extends Controller {

    @ApiOperation("index")
    public Result index() throws IOException {
        return ok();
    }
}
