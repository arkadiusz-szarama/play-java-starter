package it.szarama.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import play.db.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 02.02.2017.
 */
public class DatabaseHealthCheck extends HealthCheck {

    private final Database database;
    private final int timeout;

    public DatabaseHealthCheck(Database database, long timeoutInMs) {
        this.database = database;
        this.timeout = ((Long)TimeUnit.SECONDS.convert(timeoutInMs, TimeUnit.MILLISECONDS)).intValue();
    }

    private boolean ping(Connection connection) throws SQLException {
        return connection.isValid(timeout);
    }

    @Override
    protected Result check() throws Exception {
        if (database.withConnection(this::ping)) {
            return Result.healthy();
        }
        return Result.unhealthy("Can't ping database");
    }
}
