package it.szarama.metrics.health;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 24.05.2017.
 */
public class InternetConnectionHealthCheck extends HealthCheck {

    private final String url;
    private final int timeout;

    @Inject
    public InternetConnectionHealthCheck(String url, Long timeoutInMs) {
        this.url = url;
        this.timeout = timeoutInMs.intValue();
    }

    @Override
    protected Result check() throws Exception {
        URL url = new URL(this.url);
        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        HttpURLConnection.setFollowRedirects(true);
        huc.setConnectTimeout(timeout);
        huc.setReadTimeout(timeout);
        huc.setRequestMethod("GET");
        huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
        try {
            huc.connect();
        } finally {
            huc.disconnect();
        }
        return Result.healthy();
    }
}
