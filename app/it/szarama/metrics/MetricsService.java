package it.szarama.metrics;

import akka.actor.ActorSystem;
import com.codahale.metrics.JvmAttributeGaugeSet;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.health.jvm.ThreadDeadlockHealthCheck;
import com.codahale.metrics.json.MetricsModule;
import com.codahale.metrics.jvm.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import it.szarama.metrics.health.DatabaseHealthCheck;
import it.szarama.metrics.health.InternetConnectionHealthCheck;
import play.Configuration;
import play.Logger;
import play.api.inject.ApplicationLifecycle;
import play.db.Database;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.*;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 02.02.2017.
 */
@Singleton
public class MetricsService {
    private final static String HEALTH_CHECK_INTERVAL_KEY = "metrics.health.interval";
    private final static String INTERNET_HEALTH_CHECK_API_KEY = "metrics.health.internet.url";
    private final static String INTERNET_HEALTH_CHECK_API_DEFAULT = "https://www.google.com";
    private final static String INTERNET_HEALTH_CHECK_TIMEOUT_KEY = "metrics.health.internet.timeout";
    private final static String DATABASE_HEALTH_CHECK_TIMEOUT_KEY = "metrics.health.database.timeout";
    private final static Long DEFAULT_TIMEOUT = 30000L;
    private final static Long DEFAULT_INTERVAL = 30000L;
    private final static Long MIN_INTERVAL = 30000L;

    private final MetricRegistry metricRegistry;
    private final HealthCheckRegistry healthCheckRegistry;
    private final Configuration configuration;
    private final ApplicationLifecycle applicationLifecycle;
    private final ObjectMapper objectMapper;
    private final Database database;
    private final ActorSystem actorSystem;
    private final ExecutorService executorService;
    private final Map<String, HealthCheck.Result> lastHealthCheckResults;

    @Inject
    public MetricsService(Configuration configuration, ApplicationLifecycle applicationLifecycle, Database database, ActorSystem actorSystem) {
        this.configuration = configuration;
        this.applicationLifecycle = applicationLifecycle;
        this.database = database;
        this.actorSystem = actorSystem;

        this.metricRegistry = new MetricRegistry();
        this.objectMapper = new ObjectMapper();
        this.healthCheckRegistry = new HealthCheckRegistry();
        this.executorService = Executors.newFixedThreadPool(3);
        this.lastHealthCheckResults = new HashMap<>();

        this.initialize();
    }

    private void initialize() {
        setupJvmMetrics();
        setupHealthChecks();
        setupObjectMapper();
        setupScheduler();
        handleShutdown();
    }

    private void setupScheduler() {
        Long configInterval = configuration.getMilliseconds(HEALTH_CHECK_INTERVAL_KEY, DEFAULT_INTERVAL);
        if (configInterval!=null) {
            FiniteDuration initialDelay = Duration.create(1, TimeUnit.SECONDS);
            FiniteDuration interval = Duration.create(Math.max(configInterval, MIN_INTERVAL), TimeUnit.MILLISECONDS);
            actorSystem.scheduler().schedule(initialDelay, interval, this::scheduledHealthCheck, actorSystem.dispatcher());
        }
    }

    private void setupHealthChecks() {
        Long databaseTimeout = configuration.getMilliseconds(DATABASE_HEALTH_CHECK_TIMEOUT_KEY, DEFAULT_TIMEOUT);
        Long internetTimeout = configuration.getMilliseconds(INTERNET_HEALTH_CHECK_TIMEOUT_KEY, DEFAULT_TIMEOUT);
        String internetUrl = configuration.getString(INTERNET_HEALTH_CHECK_API_KEY, INTERNET_HEALTH_CHECK_API_DEFAULT);

        healthCheckRegistry.register("database", new DatabaseHealthCheck(database, databaseTimeout));
        healthCheckRegistry.register("deadlock", new ThreadDeadlockHealthCheck());
        healthCheckRegistry.register("internet", new InternetConnectionHealthCheck(internetUrl, internetTimeout));
    }

    private void scheduledHealthCheck() {
        if (!executorService.isShutdown()) {
            SortedMap<String, HealthCheck.Result> healthChecks = getHealthCheckRegistry().runHealthChecks(executorService);
            healthChecks.forEach((healthCheckName, result) -> {
                HealthCheck.Result lastHealthResult = lastHealthCheckResults.get(healthCheckName);
                if (lastHealthResult==null || lastHealthResult.isHealthy()!=result.isHealthy()) {
                    lastHealthCheckResults.put(healthCheckName, result);
                    if (result.isHealthy()) {
                        Logger.info("Health check for {}: {}", healthCheckName, result);
                    } else {
                        Logger.warn("Health check for {}: {}", healthCheckName, result);
                    }
                }
            });
        }
    }

    private void handleShutdown() {
        applicationLifecycle.addStopHook(() -> {
            executorService.shutdownNow();
            executorService.awaitTermination(10, TimeUnit.SECONDS);
            return CompletableFuture.completedFuture(null);
        });
    }

    private void setupObjectMapper() {
        MetricsModule metricsModule = new MetricsModule(TimeUnit.SECONDS, TimeUnit.SECONDS, false);
        objectMapper.registerModule(metricsModule);
    }

    private void setupJvmMetrics() {
        metricRegistry.register("jvm.attribute", new JvmAttributeGaugeSet());
        metricRegistry.register("jvm.gc", new GarbageCollectorMetricSet());
        metricRegistry.register("jvm.classloading", new ClassLoadingGaugeSet() );
        metricRegistry.register("jvm.fileDescriptorCountRatio", new FileDescriptorRatioGauge());
        metricRegistry.register("jvm.buffers", new BufferPoolMetricSet(ManagementFactory.getPlatformMBeanServer()));
        metricRegistry.register("jvm.memory", new MemoryUsageGaugeSet());
        metricRegistry.register("jvm.threads", new ThreadStatesGaugeSet());
    }

    public MetricRegistry getMetricsRegistry() {
        return metricRegistry;
    }

    public HealthCheckRegistry getHealthCheckRegistry() {
        return healthCheckRegistry;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public ThreadDump getThreadDump() {
        return new ThreadDump(ManagementFactory.getThreadMXBean());
    }

    public ObjectWriter getObjectWriter() {
        return objectMapper.writerWithDefaultPrettyPrinter();
    }
}
