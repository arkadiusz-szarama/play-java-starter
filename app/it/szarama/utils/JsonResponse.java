package it.szarama.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 18.01.2017.
 */
public class JsonResponse {
    private static final ObjectMapper objectMapper = new SecureObjectMapper();

    private JsonResponse() {
    }

    public static JsonNode empty() {
        return prepare(new Response<>(), null);
    }

    public static <T> JsonNode prepare(final T data) {
        return prepare(data, null);
    }

    public static <T> JsonNode prepare(final T data, final Class<?> view) {
        Response<T> response = new Response<>();
        response.setData(data);
        return prepareCleanJsonNode(response, view);
    }

    public static <T> JsonNode prepare(final Response<T> response, final Class<?> view) {
        return prepareCleanJsonNode(response, view);
    }

    public static <T> JsonNode prepareCleanJsonNode(final T rawData, final Class<?> view) {
        try {
            String data;
            if (null == view) {
                data = objectMapper.writeValueAsString(rawData);
            } else {
                data = objectMapper.writerWithView(view).writeValueAsString(rawData);
            }

            return objectMapper.readTree(data);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static JsonNode readTree(final String data) throws IOException {
        return objectMapper.readTree(data);
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Response<T> {
        private Boolean error = false;

        private String message;

        private T data;

        public Response() {
        }

        public Response(final T data) {
            this.data = data;
        }

        public Boolean getError() {
            return error;
        }

        public void setError(final Boolean error) {
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(final String message) {
            this.message = message;
        }

        public T getData() {
            return data;
        }

        public void setData(final T data) {
            this.data = data;
        }
    }
}
