package it.szarama.utils;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Created by Arkadiusz Szarama (arkadiusz@szarama.it) on 18.01.2017.
 */
public class SecureObjectMapper extends ObjectMapper {

    public SecureObjectMapper() {
        super.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
        super.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        super.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true);
    }
}
